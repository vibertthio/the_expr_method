## Modulation and sequencing

   Modulation and sequencing are two building blocks that are fundamental to sound synthesis and
   music making. The former typically aims to control the finer details and the timbre of a sound,
   whereas the latter deals with the arrangement and the structure of "musical" events over
   time. Their definitions can briefly be summarized as the following:

   Modulation, applying an continuous signal onto a parameter, so the value of the parameter changes
   smoothly over time.

   Sequencing, applying a sequence of values onto a parameter, so the value of the parameter changes
   discretely over time.

   Modulation is generally easier to understand, as it often is taught as the "introduction" to
   sound synthesis. The three properties of any given oscillating sound (frequency, amplitude and
   phase) can all be modulated to create interesting results.

   Sequencing, on the other hand, is less common to achieve using digital audio exclusively
   signal. The objective is essentially to quantize the signal into discrete "steps". As a result,
   even though the signal is continuous, the changes in its values are quantized accordingly. One
   can also think of it as the reduction in "resolution" of a given signal. One example would be to
   make a DSP version of a step counter.

   The "trick" to make such a counter is simply to multiply the signal with a (max count) variable,
   and then only to take the integer part of the result. For example: [phasor~ 1]->;[*~ 5]->;[expr~
   floor($v1, 0)] This will result to a signal that counts from 0 to 4 in one second.

   Having a good practical understanding on both modulation and sequencing is crucial in grasping
   and taking full advantage of the topics to come. The following questions can be used as starting
   points to approach the subject.

   What are the differences and commonalities between modulation and sequencing?  Are they
   essentially the same?  Where can they be applied?  EXAMPLE PATCH: 001_mod-seq.pd

## Modulation

   Modulation, can essentially be seen as applying the "shape" of one signal onto another. For example,
   modulating the amplitude of an oscillator (carrier signal) with an sawtooth wave (modulating/control
   signal) would create a crescendo in loudness with a sudden drop in volume at the end. Triangle wave,
   on the other hand would produce a crescendo and followed by a smooth decrescendo. The resulting
   sound thus follows the visual appearance of the modulating signal.

   Visualising the control signal in such a manner can be quite a intuitive process when applying
   modulations in composition.

   Supposing the control signal always starts with a sawtooth wave ([phasor~]), the aim therefore is to
   find ways to "bend" its linear ramp into other suitable/interesting shapes to be further made used
   of. Following examples will demonstrate some simple transformations in which a sawtooth wave is
   "shaped" into other useful waveforms.

   - Reverse Sawtooth: [phasor~ 1]->[-~ 1]->[abs~]
   - Triangle wave: [phasor~ 1]->[*~ 2]->[-~ 1]->[abs~]
   - Square/Pulse wave: [phasor~ 1]->[expr~ if($v1<0.5, 1, 0)]
   - Attack/Decay envelope: Example patch 002_wave-shaping.pd

   Above examples may seem trivial at first, they do however provide highly functional shapes that can be
   very useful in many common compositional scenarios.

   Exercise:

   - Use only [expr~] object for reverse Sawtooth and Triangle wave
   - The Triangle wave above is not entirely correct, how can it be fixed?
   - How to do simple Pulse Width Modulation from the given example above?

## Sequencing

   Apart from the discrete changes in values, generating control signals for sequencing, can be
   approached just the same way as that for modulation. That is to say, one essentially find ways to
   "alter" the appearance of a given waveform so that a "stepped" version of it emerges as a
   result. Transforming a signal from continuously and smoothly changing to being quantised and
   value-restricted is therefore the objective when preparing control signals for sequencing.

   The following quick example will produce a sawtooth signal that contains 10 distinct steps within
   its wave cycle.


   [phasor~ 1]->[expr~ floor($v1*10, 0)/9]

   Because a quantised signal holds the same value temporarily until the next element comes along, by
   using comparison operators ("equals for example"), it becomes possible to isolate/select a certain
   portion of the sequence. Since the comparative statements will return true for the entire duration
   of given time "step", it thus allow for further (differential) treatments to the signal that falls
   on either side of the test. This property of the sequencing signal might initially seem trivial, but
   it makes such a signal very useful in solving many of the tasks that are commonly faced when dealing
   with time dependent structures.

   The following example demonstrates how one can select the last element in the sequence of 4
   incremental counts (0,1,2,3,0,1,2,3...), and subsquently reassign its output value
   (0,1,2,0,0,1,2,0...).


   [phasor~ 1]->[expr~ floor($v1*4, 0)]->[expr~ if($v1 == 3, 0, $v1)]

   Combining with relational tests, control signal for sequencing can extend far beyond as being a
   simple counting mechanism. Many common objects such as [select], [spigot], [int] and [moses] can all
   be emulated easily.  Dynamic signal routing and flexiable looping are other examples that are
   relatively uncomplicated to implement.

   The exmple below demonstrates an interesting way of counting by utilising the modulo function.

   Exercise:  

   - What other arithmetic functions can be used to modify the control signal?
   - Which other parts of the example patch can be modulated/sequenced?

## Unit of time
   Time units, here refers to the duration of one single musical event. It is relative, in that a
   selected unit can be subdivided into smaller parts, as well as summed up to form a larger
   section. That is to say, if the time unit is measured in quarter notes, the sum of four units will
   make up for one bar, and half of the unit would be an eighth note. If the time unit were expressed
   as one bar, four of which together could resolve to a musical phrase, and a semiquaver would be one
   sixteenth of the unit duration.

   To understand timing in this manner is extremely helpful when making compositional arrangements
   using DSP control signals. As long as one is able to express the required timing in terms of
   proportions relating the chosen time units, simple arithmetic would be all it takes to implement the
   solution.

   For instance, if one period of a sawtooth wave is considered as one bar, to produce the same
   waveform but at the rate of quarter notes would simply be:

   [phasor~ 1]->[expr~ fmod($v1*4, 1)]->[send~ QTR_NOTES]
              \
               ->[send~ ONE_BAR]

   This works by first multiplying the original signal with an desired amount, in order to scale up the
   "rate of change" appropriately. It is then passed through the fmod (Floating Point Modulo)
   function. By having the second argument set to 1, fmod essentially discard the integer portion of
   its input. This results to a signal which has the amplified increment, but restricted to floating
   point values between 0 and 1.

   To gain any other subdivisions of one bar at 1Hz, simply replace the multiplication factor with
   another value. Thus, one can quickly produce multiple and parallel timing signal from just one
   [phasor~] object.

   However, if one period of a sawtooth is decided to be one beat (quarter notes, for example), to
   obtain the timing signal for one bar and beyond would require a little more thought, but otherwise
   based on the same principle as above.

   [phasor~ 0.25]->[expr~ fmod($v1*4,1)]->[send~ QTR_NOTES]
                 \
                  ->[send ONE_BAR]

   This works by first dividing the frequency of the starting [phasor~ ] object, to reduce its rate by
   an appropriate amount (in this case, 4 times slower). The slowed down oscillator can then be used as
   the "extended" timing. But, to regain the sawtooth of 1Hz as one beat, the same fmod trick is again
   used.

   In short, subdividing a given time unit does not change the frequency of the original control
   signal. Whereas timing extension involves reducing the frequency of the initial control oscillator.

   Exercise:
   - All the examples above measures time in Hz (1 period of 1Hz is 1 second long), how could this be
        translated to use more conventional measurement such as BPM?

## Serial and parallel
   Modulation, sequencing, division of time unit are simple ways to utilise a (Sawtooth) signal in
   order to achieve certain functions. How in which they are put together therefore determines the
   "arrangement" of the composition.

   One simple approach in combining these elementary components, is to deduce the possibilities into
   two basic types: serial and parallel.

   Serial, implies putting these functions one after another, so to produce desired outcome at the
   end of the chain. It typically results to control signals for modulation, or for logic
   control. The intermediate steps, in this case, involves resolving the correct timing
   frequency. In the example below, the fmod() is first used to divide the wave period of the
   incoming sawtooth, and then passed onto the next [expr~] to produce triangle wave.

   [phasor~ ]->[expr~ fmod($v1*4, 1)]->[expr~ abs(abs($v1-0.5)-0.5)*2]

   Parallel, on the other hand, refers to the diverging of signals, so that branches are created with
   each of the strand serving a different purpose. A common usage of this set-up would be to control
   various aspects of a given sound synthesis algorithm, from within a same time scope. For example, by
   using fmod() and floor() from the same timing signal, one can produce amplitude envelopes, as well
   as counters to alter the frequency of an oscillator at the same time. This process is very much the
   same as how control voltage (CV) is used in analog synthesiser.

   [phasor~ ]->[expr~ fmod($v1*4, 1)]->[expr~ abs(abs($v1-0.5)-0.5)*2]->[send~ amp_env]
             \ ->[expr~ floor($v1*4, 0)]->[expr~ if($v1%2, 2,1)]->[send~ freq]

   As seen in above examples, serial connections are generally not very long, unless multiple steps
   are taken to derive the necessary timing. By grouping multiple serial process, a parallel
   arrangement is thus formed. Moreover, different group of parallel process can also be
   interconnected in a serial fashion, so to manipulate the composition at different scope of time.

   In short, such a generalisation are guide lines in which can be used as a starting point when
   applying these techniques in composition. One other useful way in approaching the subject would
   be seeing it essentially the same as how CV signals are used analogue modular synthesiser.

   Exercise:
   - Using the example patch, extend it into a 4 bar phrase.
   - Using the example patch, make other voice/concurrent parts in the arrangement.

## Random elements
   Having random values are often a quick and easy method to create variations in compositional
   arrangements. Conventionally, the [random] object is used for this purpose. By triggering the
   object with a bang, it would output a new value within a specifiable range.

   Random values can also be obtained using only audio control signals, which involves the use of
   sample and hold function ([samphold~] in Pd). Generally speaking, it takes a snapshot of its
   input/source signal, and output the sample value until it samples again. The second inlet of
   [samphold~], therefore, controls how often the new samples are taken. In this case, it is when the
   signal to the second inlet has a negative transition/increment.

   For example, if a [phasor~] is connected to the second input, [samphold~] will be triggered right at
   the boundary between each wave cycle. This is because only at that point, the increment of [phasor~]
   would be negative. This unique property of sawtooth wave enables [samphold~] to take one-off samples
   at regular interval.

   Once the principle of [samphold~] is understood, simply connecting a [noise~] object to its first
   input would result to a randomized signal. The random values would consequently change according to
   the signal characteristics given to the second inlet. Simple numerical manipulation might be
   necessary in order to scale the values to a appropriate range thereafter.

   Exercises:
   - Besides using [noise~] as a source for randomised signal, what other use might [samphold~] also
     have?
   - What other wave forms might also be interesting to be used as the control signal for
     [samphold~]?

## Modulus Djent
   The modulo operator can be useful in making (pseudo) generative patterns. Not only its relatively
   easy to use, the process is also responsive to parameter changes. In other words, complex structures
   can be derived from simple operations, the transitions between perceivingly different patterns can
   also be quickly made.

   The following two examples illustrates a simple usage of modulo and the resulting patterns:

   if($v1%4 == 0, 1, 0):
   COUNTER: 0 1 2 3 4 5 6 7 8 9 A B C D E F . . . .
   PATTERN: 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 . . . .

   if($v1%5 == 0, 1, 0):
   COUNTER: 0 1 2 3 4 5 6 7 8 9 A B C D E F . . . .
   PATTERN: 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 . . . .

   Such implementation don't produce interesting results by itself, unless the patterns are
   stacked/layered against each other.

   However, by chaining modulo operators one after another, the result quickly becomes more
   complex. For example:

   if(($v1%5)%3 == 0, 1, 0):
   COUNTER: 0 1 2 3 4 5 6 7 8 9 A B C D E F . . . .
   PATTERN: 1 0 0 1 0 1 0 0 1 0 1 0 0 1 0 1 . . . .

   if((($v1%7)%4)%3 == 0, 1, 0):
   COUNTER: 0 1 2 3 4 5 6 7 8 9 A B C D E F . . . .
   PATTERN: 1 0 0 1 1 0 0 1 0 0 1 1 0 0 1 0 . . . .

   if((($v1%11)%7)%4 == 0, 1, 0):
   COUNTER: 0 1 2 3 4 5 6 7 8 9 A B C D E F . . . .
   PATTERN: 1 0 0 0 1 0 0 1 0 0 0 1 0 0 0 1 . . . .


   The general principle is thus to use progressively smaller values after each modulo operator, so to
   nest smaller counts inside of larger loops. Often, these divisors, especially towards smaller ones,
   can be modulated by other time measure, so to increase the diversity of the resulting pattern.

   Furthermore, since largest divisor determines the length of the entire pattern, by modulating it
   would also produce different pattern, even if the rest of the expression remains the same. Consider
   the following:

   if(($v1%5)%3 == 0, 1, 0) vs. if(($v1%7)%3 == 0, 1, 0):
   COUNTER: 0 1 2 3 4 5 6 7 8 9 A B C D E F . . . .
   PATTERN: 1 0 0 1 0 1 0 0 1 0 1 0 0 1 0 1 . . . .
   PATTERN: 1 0 0 1 0 0 1 0 0 1 0 0 1 0 1 0 . . . .

   Notice that the examples so far uniformly select the first count to give the final structure, but
   the following can also be considered:

   if(($v1%5)%3 == $v2)
   where $v2 is a modulated signal to select which element
   in the count is to be used.

   Here is an quick example using modulo operator to make Djent:)

   Exercise:
   - What other ways can modulo be useful to make/manipulate patterns?
   - How to use phase shift to further process the resulting pattern?

## Low pass filter ramp

   By default, functions such as floor() or if() would produce a "hard" stepped signal. That is to
   say, signal changes abruptly from one level to the next, governed by predefined functions and
   timing. Whilst this behaviour is absolutely expected and correct, it may not results to the most
   "musical" effect at times. In other words, being able to move seamlessly from one value to another
   can contributes to a more "natural" quality to the composition. Musically, such an effect can be
   seen as "portamento", where by the pitch slides from note to note, rather than stepped.

   A simple one-pole low pass filter can be used in this case to "smooth" out incoming signal, making
   it much less abrupt. consider the following:

   [phasor~ 0.5] -> [expr~ (floor($v1*4, 0)+1)*200] -> [osc~] -> [dac~]
   and

   [phasor~ 0.5] -> [expr~ (floor($v1*4, 0)+1)*200] -> [lop~ 10] [osc~] -> [dac~]

   The value 10 given as the argument to [lop~] specifies the filter's cut-off frequency, and in this
   case, it translates to the time taken to smooth out the incoming signal. For example, 1Hz would
   equates to 1 second in time, whereas 10Hz would only take 100 millisecond.

   It is also worth noting that if the frequency given to [lop~] is lower/slower than the rate of the
   main timing, the resulting signal may never fully reach its target value. In this case, it would
   simple wobble along between different values.

   Another use case for [lop~] would be a quick-and-dirty envelop set-up, such as:

   [phasor~ 1] -> [expr~ if($v1&lt;0.5, 1, 0)] -> [lop~ 5]

   Ordinarily, such type of "gate" switching would produce clicks at the edges of the on
   state. However, by passing the signal through a low pass filter, the hard edges thus are rounded
   off, making the transitions much less harsh.

   The following example shows an simple riff using 2 [lop~]:


   Exercise:
    - Are there any other use case for [lop~]
    - What are the drawback of the [lop~] object in Pd used in this context?
    - How about using other types of filters?

## Phase shifting

   Previously, the fmod() function had been introduced in the context of frequency multiplication of a
   given sawtooth signal. For example:

   [phasor~ 0.25] -> [expr~ fmod($v1*4, 1)]

   Besides this, fmod() can also be used to shift the phase position of a given sawtooth signal. The
   principle is largely the same as the example above, apart from replacing the multiplication with
   addition, which signifies the amount of displacement. For example:

   [phasor~ 0.25] -> [expr~ fmod($v1+(1/4.0), 1)]

   This will push the phase position of the input sawtooth by a quarter of the total wavelength.

                                             Original phase position
     .    /|       /|       /|       /|
     .   / |      / |      / |      / |
     .	/  |     /  |     /  |     /  |
     . /   |    /   |    /   |    /   |
     ./    |   /    |   /    |   /    |
     /     |  /     |  /     |  /     |
    /.     | /      | /      | /      |
   / .     |/       |/       |/       |
     .                                       Phase shift by 25%
     .       /|       /|       /|       /|
     .      / |      / |      / |      / |
     .     /  |     /  |     /  |     /  |
     .    /   |    /   |    /   |    /   |
     .   /    |   /    |   /    |   /    |
     .  /     |  /     |  /     |  /     |
     . /      | /      | /      | /      |
     ./       |/       |/       |/       |



     Musically speaking, it essentially adds a time delay/offset to the original control signal. One way
     this can be useful is to arrange the shifted signals in parallel, which would produce effects
     similar to a musical canon. This enables the ability to generate interesting compositional
     variations from a limited pre-defined sequence, for example. Furthermore, the amount of shifting can
     also be modulated just the same way as amplitude and frequency, which allows for more dynamic and
     responsive structures to emerge.
    
     For controlling sound synthesis, phase shifting provides a simple mechanism to move envelope and
     logic level along the time axis. Consider the following:
    
     [phasor~ 0.25] -> [expr~ if($v1 [s~ signal_1]
                   \-> [expr~ fmod($v1+0.25, 1)] -> [expr~ if($v1 [s~ signal_2]
    
     signal_1 produces a gate switching signal with 25% duty cycle. By shifting the phase of the
     [phasor~], signal_2 would have the same rate of change, but delayed by quarter of the total
     wavelength.
    
     It is worth mentioning that, attention needs to be paid to the total amount of shift that are
     "allowed". that is to say, the "effective" shift permitted would be the total wave period minus the
     duty cycle of the gate. In the case of above example, if the amount of shift had been greater than
     0.75, the "on" portion of the gate would be pushed off the right edge, and thus may cause undesired
     effect.


     Exercise:
     How to shift the phase of a sine wave?

## Shuffle
   When creating subdivisions of time with fmod() and floor(), it would produce equal segments by
   default. This is often convenient for further processing, because the duration of each measures are
   identical. However, it may not always be so musically desirable to derive time intervals in such a
   uniformed manor. Often, a slight shuffle is intentionally introduced into repeating rhythmic
   patterns, so to prevent the end result from sounding too static. This is generally referred to as
   the "swing".

   To shuffle a sawtooth signal, the following method can be used:

   [phasor~ 0.5]                                 [sig~ 0.6]
   |                                             |
   [expr~ if($v1&lt;$v2, $v1/$v2, ($v1-$v2)/(1-$v2))]
   |
   [s~ shuffled_signal]

   The second inlet($v2) of the [expr~] signifies the amount, as well as the direction, of the
   shuffle. When set to 0.5, the input phasor would be divided equally, where as 0.6 would increase the
   duration of the first section, making it a little be longer than the second segment. Note that the
   allowed value for $v2, in this case, would be between 0 and 1.

   Once a sawtooth signal can be halved in arbitrary proportion, it would also be useful to be able to
   identify the current position of the signal, in relation to the two uneven portions of the "swing".

   To to this, the following can be used in combination with the previous example.

   [phasor~ 0.5]           [sig~ 0.6]
   |                       |
   [expr~ if($v1&lt;$v2, 0, 1)]
   |
   [s~ shuffle_idx]

   In this case, the two sections can now be identified with an index value of 0 and 1. It is worth
   noting that, with shuffled signal, it would be more difficult to obtain a counting signal from it
   using floor(). If a specific segment of the shuffled signal needs to be isolated within a larger
   musical phrase, for example, a combination of normal floor() counter and the above mentioned
   indexing can be used to achieve that. This can be seen in the example patch below.

   The musical effect in which shuffling creates is very much similar to syncopation. By intentionally
   distributing the time units unevenly, an off-beat "groove" sensation can be perceived from
   it. However, with syncopated rhythms, the amount of "shuffle" would still fall into the evenly
   divided time subdivisions. With shuffling, on the other hand, would usually be a adjustable amount
   (as found in many drum machine), usually a percentage of the current time unit/measurement. For
   example, given two consecutive 4 quarter notes, a swing of 10% can be set, so that the first beat
   are slightly longer than the second.

## Playing samples
   Although the examples so far all uses simple sound synthesis to demonstrate the underlying
   principles of the signal-only methodology, they can easily be extended to control and manipulate
   audio samples as well.

   As sound files are in essence arrays of values stored on disk, playing back an sample can be done
   simply by using a [phasor~] to systematically step through each of the values in the file.

   To begin with, one must know the length of the chosen file, in number of samples. This value can be
   obtained from the output of the [soundfiler] object, and can then be multiplied with a sawtooth
   signal. The result is a complete indexing from the beginning to the end of the file, which the
   [tabread~] object can accept as input.

   To resolve the "correct" rate for playback, however, can be somewhat ambiguous. On the one hand,
   there is the actual speed in which the sample was first recorded. On the other hand, there is the
   desired speed in which the current composition demands.

   Assuming the goal is to playback at the recorded speed, the original sample rate also must be
   known. By dividing the length of the sound file with the sample rate, the time duration is therefore
   known, and can easily be converted into frequency.

   The following example shows an simple playback setup

   Such a result admittedly is not very interesting compositionally. However, by applying the
   principles that had been introduced previously, step counting for instance, one can easily make the
   necessary changes to give much more flexibility and possibilities.

   This second example demonstrates the ability to slice the sample into definable sections, and
   rearrange them according to a modulated counter.


   Note that there is no amplitude envelope in the above example to correct the hard clipping at the
   edges of each sample sections. To achieve this, one can produce an signal in the shape of
   (isosceles) trapezoid signal to modulate over the sample values.

  Exercises:
  - Besides the frequency, what other parameters can be modified/modulated for sample playback?
  - Are there other interesting counting method to experiment with?

## Trapezoid
   Some wave forms, like sawtooth, are easy to implement and provides useful building blocks in
   signal manipulation. Trapezoid is another such example.

   It is particularly suitable used as amplitude envelope, in which smooth attack and decay can be
   guaranteed, whilst the "on" portion of the envelope stays fully open for as long as possible. For
   instance, if an triangle wave had been used, the envelope will never stay fully open, as the
   signal starts to ramp down as soon as it reaches the top. As a result, modulated signal can
   appear to be continuously fading in and out, or the lost of overall "loudness". Trapezoid, on the
   other hand, provides an convenient method to address these issues.

   Trapezoid, in fact, can be easily constructed out of a triangle wave, with two additional
   operates involved. The triangle is first scaled up (multiplication) by a desirable amount, and
   than clipped(min()) at the maximum y-axis value of the envelope, which conventionally is 1. The
   more the signal is scaled, the steeper the sides of the resulting trapezoid becomes, thus shorter
   in attack/decay duration.

   One other variation would be to use the pow() function, turning the liner slopes of the
   (inverted) triangle into exponential curves. The envelope is then flipped over to the right
   orientation for use. The advantage of this method is that, given the same breakpoint locations,
   the total "on" area of the signal would be larger than that of the linear trapezoid. The
   resulting modulation would have even less perceived loudness loss. However, such a shape
   resembles more of a classic jello than a trapezoid.

   In the previous post on sample play back, where a given sound file are splitted into multiple
   sections, clipping can often occur at the edges of the segments due to non zero-crossing. This is
   an ideal scenario to apply trapezoid, or equivalent variants, to rectify such error, by using it
   as a amplitude envelope.

   Exercise:

   Are there other types of slops/curves that can be used in the waveform transformation similar to
   the exmple above?

## Topics
  * BASIC
  * intermediate
   - structure generator
   - [expr~] multiple output
